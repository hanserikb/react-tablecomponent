var data = {
  headers: [{
    name: 'ID',
    key: 'id',
    disableEdit: true
  }, {
    name: 'Name',
    key: 'name'
  }, {
    name: 'Years',
    key: 'age'
  }, {
    name: 'Salary',
    key: 'income',
    cellRenderer: function(item, key) {
      return '$ ' + (item[key] || 0);
    },
    onClick: function(e) {
      e.target.addClass('clicked');
    }
  }],
  data: [{
    id: '1',
    name: 'My name',
    age: '12'
  }, {
    id: '2',
    name: 'My other name',
    age: '33'
  }, {
    id: '3',
    name: 'Johnnie',
    age: '22',
    income: 30000
  }]
};

var Table = React.createClass({
  displayName: 'table',

  /** React stuff **/
  propTypes: {
    headers: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    data: React.PropTypes.arrayOf(React.PropTypes.object).isRequired
  },
  getInitialState: function() {
    return {
      data: this.props.data,
      headers: this.props.headers,
      edit: null,
      isSearching: false
    }
  },

  /**private methods**/
  _sort: function(key) {
    var data = this.state.data.slice();
    this.asc = !this.asc;
    data.sort(function(a, b) {
      if (a[key] < b[key]) {
        return -1;
      } else if (a[key] > b[key]) {
        return 1;
      } else {
        return 0;
      }
    });
    this.setState({
      data: this.asc ? data : data.reverse()
    });
  },
  __showEditor: function(dataIndex, headerKey, e) {
    this.setState({
      edit: {
        cell: headerKey,
        row: dataIndex
      }
    })
  },
  __exitEditing: function(extendState) {
    var newState = {};
    for (prop in extendState) {
      newState[prop] = extendState[prop];
    }
    newState.edit = null;
    this.setState(newState);
  },
  __save: function(e) {
    e.preventDefault();
    var inputValue = e.target.firstChild.value;
    var data = this.state.data.slice();
    data[this.state.edit.row][this.state.edit.cell] = inputValue;
    this.__exitEditing({data: data});
  },
  __renderToolbar: function(){
    return <div className="toolbar"><button onClick={ this.__toggleSearch }>Search</button></div>
  },
  __toggleSearch: function() {
    this.setState({
      isSearching: !this.state.isSearching
    });
  },
  __renderSearch: function() {
    if (!this.state.isSearching) {
      return null;
    } else {
      return (<tr onChange={this._search}>
        { this.props.headers.map(function(header, idx) {
          return <td key={idx}>
            <input type="text" data-key={header.key} />
          </td>
        })}
      </tr>)
    }
  },
  __renderTable: function() {
    var that = this;
    return (
        <table className='table'>
          <thead>
          <tr>{ this.state.headers.map(function(header) {
            return <th key={header.key}
                       onClick={that._sort.bind(that, header.key)}>{ header.name }</th>
          }) }</tr>
          </thead>
          <tbody>
          { this.__renderSearch() }
          { that.state.data.map(function(item, dataIndex) {
            return (<tr key={'table-row-' + dataIndex }>
              { that.state.headers.map(function(header) {
                /**
                 * Gets the string to render by running the cellRenderer or grabbing the passed header property
                 */
                var display = (function() {
                  if (typeof header.cellRenderer === 'function') {
                    return header.cellRenderer(item, header.key);
                  } else {
                    return item[header.key];
                  }
                })();

                // Should we render an input field or not?
                var edit = that.state.edit;
                var ret;
                if (!header.disableEdit && edit && edit.row === dataIndex && edit.cell === header.key) {
                  ret = (<form onSubmit={that.__save}>
                    <input type="text" defaultValue={item[header.key]} autofocus />
                  </form>);
                } else {
                  ret = display;
                }

                // The table data cell
                return (<td
                    onBlur={ that.__exitEditing }
                    onDoubleClick={ that.__showEditor.bind(that, dataIndex, header.key )}
                    key={ header.key }
                >{ret}</td>);
              })}
            </tr>);
          })}</tbody>
        </table>)
  },

  /** Render all the things! **/
  render: function() {
    return <div>{ this.__renderToolbar() }{ this.__renderTable() }</div>;
  }
});
ReactDOM.render(<Table headers={data.headers} data={data.data}></Table>, document.getElementById('app'));
